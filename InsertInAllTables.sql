use hotel_rot;

INSERT INTO zimmerart (typ) VALUES ("grosse Suite");
INSERT INTO zimmerart (typ) VALUES ("kleine Suite");

INSERT INTO zimmer (zimmer_zimmernr, nichtraucher, stockwerk, alpenblick, minibar, zimmerart_id,
  standort, zimmergroesseQuadratmeter) VALUES (32, true, 2, true, true, 1, "Hauptgebauede", 80);
INSERT INTO zimmer (zimmer_zimmernr, nichtraucher, stockwerk, alpenblick, minibar, zimmerart_id,
  standort, zimmergroesseQuadratmeter) VALUES (43, false, 3, true, false, 2, "Hauptgebauede", 20);

INSERT INTO bettart (typ) VALUES ("Einzelbett");

INSERT INTO zimmer_bettart (bettart_id, zimmer_zimmernr) VALUES (1, 32);
INSERT INTO zimmer_bettart (bettart_id, zimmer_zimmernr) VALUES (1, 43);

INSERT INTO sanitaerobjekt (beschreibung) VALUES ("WC");
INSERT INTO sanitaerobjekt (beschreibung) VALUES ("Badewanne");
INSERT INTO sanitaerobjekt (beschreibung) VALUES ("Dusche");

INSERT INTO sanitaerobjekt_zimmer (sanitaerobjekt_id, zimmer_zimmernr) VALUES (1, 32);
INSERT INTO sanitaerobjekt_zimmer (sanitaerobjekt_id, zimmer_zimmernr) VALUES (1, 43);
INSERT INTO sanitaerobjekt_zimmer (sanitaerobjekt_id, zimmer_zimmernr) VALUES (2, 32);
INSERT INTO sanitaerobjekt_zimmer (sanitaerobjekt_id, zimmer_zimmernr) VALUES (2, 43);

INSERT INTO beschreibung (zimmer_zimmernr) VALUES (32);
INSERT INTO beschreibung (zimmer_zimmernr) VALUES (43);

INSERT INTO textbaustein (textbaustein, beschreibung_id) VALUES ("Schoenes Zimmer", 1);
INSERT INTO textbaustein (textbaustein, beschreibung_id) VALUES ("Dreckiges Zimmer", 2);

INSERT INTO foto (name, url, beschreibung_id) VALUES ("Fensteraussicht", "www.bestfotos.org", 1);
INSERT INTO foto (name, url, beschreibung_id) VALUES ("Fensteraussicht", "www.bestfotos.org", 2);

INSERT INTO adresstyp (adresstyp_id, bezeichnung) VALUES (1, "Privat");
INSERT INTO adresstyp (adresstyp_id, bezeichnung) VALUES (2, "Geschaeftlich");

INSERT INTO adresse (adresse_id, plz, ort, land, strasse, hausnr, adresstyp_id) VALUES (1, "8000", "Zuerich", "CH", "Bahnhofstrasse", "15", 1);
INSERT INTO adresse (adresse_id, plz, ort, land, strasse, hausnr, adresstyp_id) VALUES (2, "1234", "Exampletown", "US", "Blasquare", "48-b", 1);
INSERT INTO adresse (adresse_id, plz, ort, land, strasse, hausnr, adresstyp_id) VALUES (3, "7000", "Chur", "CH", "Postplatz", "1", 2);
INSERT INTO adresse (adresse_id, plz, ort, land, strasse, hausnr, adresstyp_id) VALUES (4, "7203", "Trimmis", "CH", "Gartenweg", "24", 1);

INSERT INTO person (person_id, vorname, nachname, geburtsdatum, sprache, adresse_id) VALUES (1, "Giancarlo", "Bergamin", '94-08-10', "de", 1);
INSERT INTO person (person_id, vorname, nachname, geburtsdatum, sprache, adresse_id) VALUES (2, "Max", "Muster", '90-01-01', "en", 2);
INSERT INTO person (person_id, vorname, nachname, geburtsdatum, sprache, adresse_id) VALUES (3, "Vanessa", "Suter", '99-09-08', "de", 4);
INSERT INTO person (person_id, vorname, nachname, geburtsdatum, sprache, adresse_id) VALUES (4, "Helene", "Fischer", '79-04-14', "de", 3);
INSERT INTO person (person_id, vorname, nachname, geburtsdatum, sprache, adresse_id) VALUES (5, "Urs", "Mueller", '55-04-14', "de", 3);

INSERT INTO ahv (ahv, person_id) VALUES (123455667, 1);
INSERT INTO ahv (ahv, person_id) VALUES (089865434, 1);
INSERT INTO ahv (ahv, person_id) VALUES (756888567, 2);
INSERT INTO ahv (ahv, person_id) VALUES (934567894, 3);
INSERT INTO ahv (ahv, person_id) VALUES (454457467, 4);
INSERT INTO ahv (ahv, person_id) VALUES (454333337, 5);

INSERT INTO telefonTyp (telefontyp_id, beschreibung) VALUES (1, "Festnetz");
INSERT INTO telefonTyp (telefontyp_id, beschreibung) VALUES (2, "Mobil");
INSERT INTO telefonTyp (telefontyp_id, beschreibung) VALUES (3, "Fax");

INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0791234455, 1, 2);
INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0799987654, 1, 1);
INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0796655443, 2, 2);
INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0791445567, 3, 2);
INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0795566774, 4, 2);
INSERT INTO telefon (nummer, person_id, telefontyp_id) VALUES (0793333334, 5, 2);

INSERT INTO email (emailadresse, person_id) VALUES ("test@test.com",1);
INSERT INTO email (emailadresse, person_id) VALUES ("max.muster@test.com",2);
INSERT INTO email (emailadresse, person_id) VALUES ("suter.v@test.com",3);
INSERT INTO email (emailadresse, person_id) VALUES ("master@bla.com",4);

INSERT INTO rolle (rolle_id, bezeichnung, rabatt) VALUES (1,"Mitarbeiter", True);
INSERT INTO rolle (rolle_id, bezeichnung, rabatt) VALUES (2,"Gast", False);
INSERT INTO rolle (rolle_id, bezeichnung, rabatt) VALUES (3,"Ansprechpartner", False);

INSERT INTO person_rolle (person_id, rolle_id) VALUES (1,1);
INSERT INTO person_rolle (person_id, rolle_id) VALUES (2,1);
INSERT INTO person_rolle (person_id, rolle_id) VALUES (3,2);
INSERT INTO person_rolle (person_id, rolle_id) VALUES (4,3);
INSERT INTO person_rolle (person_id, rolle_id) VALUES (5,3);

INSERT INTO kreditkarte (kreditkartennr, geheimzahl, valid, nachname, vorname) VALUES (123456789, 123, "17/05", "Bergamin", "Giancarlo");
INSERT INTO kreditkarte (kreditkartennr, geheimzahl, valid, nachname, vorname) VALUES (456789123, 456, "17/07", "Muster", "Max");
INSERT INTO kreditkarte (kreditkartennr, geheimzahl, valid, nachname, vorname) VALUES (987654321, 789, "19/05", "Suter", "Vanessa");
INSERT INTO kreditkarte (kreditkartennr, geheimzahl, valid, nachname, vorname) VALUES (777654321, 976, "21/02", "Fischer", "Helene");

INSERT INTO reisegruppe (reisegruppe_id) VALUES (1);
INSERT INTO reisegruppe (reisegruppe_id) VALUES (2);


INSERT INTO person_reisegruppe (person_id, reisegruppe_id) VALUES (1,1);
INSERT INTO person_reisegruppe (person_id, reisegruppe_id) VALUES (2,1);
INSERT INTO person_reisegruppe (person_id, reisegruppe_id) VALUES (3,2);
INSERT INTO person_reisegruppe (person_id, reisegruppe_id) VALUES (4,2);

INSERT INTO vermittlungsunternehmenTyp (vermittlungsunternehmenTyp_id, bezeichnung) VALUES (1, "Reiseunternehmen");
INSERT INTO vermittlungsunternehmenTyp (vermittlungsunternehmenTyp_id, bezeichnung) VALUES (2, "Sonstige");

INSERT INTO vermittlungsunternehmen (vermittlungsunternehmen_id, bezeichnung, person_id, vermittlungsunternehmenTyp_id) VALUES (1,"Tui", 3, 1);
INSERT INTO vermittlungsunternehmen (vermittlungsunternehmen_id, bezeichnung, person_id, vermittlungsunternehmenTyp_id) VALUES (2,"Globus", 5, 1);

INSERT INTO person_vermittlungsunternehmen (vermittlungsunternehmen_id, person_id) VALUES (2,5);
INSERT INTO person_vermittlungsunternehmen (vermittlungsunternehmen_id, person_id) VALUES (1,3);

INSERT INTO buchung (buchungsnr, buchung_zimmer_zimmernr, buchung_person_id, von, bis, anzahl_personen, bestaetigt, buchung_reisegruppe_id, kreditkarte_kreditkartennr) 
VALUES (1, 32, 1, '17-05-05','17-05-07', 1, true, 1, 123456789);
INSERT INTO buchung (buchungsnr, buchung_zimmer_zimmernr, buchung_person_id, von, bis, anzahl_personen, bestaetigt, buchung_reisegruppe_id, kreditkarte_kreditkartennr) 
VALUES (2, 43, 2, '17-06-05','17-06-13', 1, true, 1, 456789123);

INSERT INTO buchungsstatus (buchungsstatus_id, von, bis, bezeichnung, buchung_bunchungsnr) VALUES (1,'17-05-05','17-05-07', "bestaetigt", 1);

