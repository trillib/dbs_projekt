CREATE TABLE zimmerart 
(zimmerart_id INT AUTO_INCREMENT
, typ VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (zimmerart_id)
);

CREATE TABLE zimmer 
(zimmer_zimmernr INT
, nichtraucher BOOLEAN
, stockwerk INT
, alpenblick BOOLEAN
, minibar BOOLEAN
, zimmerart_id int
, standort VARCHAR(45)
, zimmergroesseQuadratmeter INT
, CONSTRAINT pkey PRIMARY KEY (zimmer_zimmernr)
, CONSTRAINT zimmerart FOREIGN KEY (zimmerart_id) REFERENCES zimmerart (zimmerart_id)
);

CREATE TABLE beschreibung 
(beschreibung_id INT AUTO_INCREMENT
, zimmer_zimmernr INT
, CONSTRAINT pkey PRIMARY KEY (beschreibung_id)
, CONSTRAINT zimmer FOREIGN KEY (zimmer_zimmernr) REFERENCES zimmer (zimmer_zimmernr)
);

CREATE TABLE foto 
(foto_id INT AUTO_INCREMENT
, name VARCHAR(45)
, url VARCHAR(80)
, beschreibung_id INT
, CONSTRAINT pkey PRIMARY KEY (foto_id)
, CONSTRAINT beschreibung FOREIGN KEY (beschreibung_id) REFERENCES beschreibung (beschreibung_id)
);

CREATE TABLE textbaustein
(textbaustein_id INT AUTO_INCREMENT
, textbaustein VARCHAR(200)
, beschreibung_id INT
, CONSTRAINT pkey PRIMARY KEY (textbaustein_id)
, CONSTRAINT beschreibung_fkey FOREIGN KEY (beschreibung_id) REFERENCES beschreibung (beschreibung_id)
);

CREATE TABLE bettart 
(bettart_id INT AUTO_INCREMENT
, typ VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (bettart_id)
);

CREATE TABLE zimmer_bettart
(bettart_id INT
, zimmer_zimmernr INT 
, CONSTRAINT pkey PRIMARY KEY (bettart_id, zimmer_zimmernr)
);

CREATE TABLE sanitaerobject
(sanitaerobject_id INT
, beschreibung VARCHAR(45) 
, CONSTRAINT pkey PRIMARY KEY (sanitaerobject_id)
);

CREATE TABLE sanitaerobject_zimmer
(sanitaerobject_id INT AUTO_INCREMENT
, zimmer_zimmernr INT
, CONSTRAINT pkey PRIMARY KEY (sanitaerobject_id, zimmer_zimmernr)
);



