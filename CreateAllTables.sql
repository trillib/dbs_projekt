DROP SCHEMA hotel_rot;
CREATE SCHEMA hotel_rot;
USE hotel_rot;


CREATE TABLE adresstyp
(adresstyp_id INT AUTO_INCREMENT
, bezeichnung VARCHAR(45)
, CONSTRAINT pk PRIMARY KEY (adresstyp_id)
);
CREATE TABLE adresse
(adresse_id INT AUTO_INCREMENT
, plz VARCHAR(45)
, ort VARCHAR(45)
, land CHAR(2)
, strasse VARCHAR(45)
, hausnr VARCHAR(45)
, adresstyp_id INT
, CONSTRAINT pk PRIMARY KEY (adresse_id)
, CONSTRAINT adresse_adresstyp FOREIGN KEY (adresstyp_id) REFERENCES adresstyp (adresstyp_id)
);


CREATE TABLE person
(person_id INT AUTO_INCREMENT
, vorname VARCHAR(45)
, nachname VARCHAR(45)
, geburtsdatum date
, sprache CHAR(2)
, adresse_id INT
, CONSTRAINT pk PRIMARY KEY (person_id)
, CONSTRAINT person_adresse FOREIGN KEY (adresse_id) REFERENCES adresse (adresse_id)
);
CREATE TABLE ahv
(ahv INT
, person_id INT
, CONSTRAINT pk PRIMARY KEY (ahv)
, CONSTRAINT ahv_person FOREIGN KEY (person_id) REFERENCES person (person_id)
);
CREATE TABLE telefonTyp
(telefontyp_id INT AUTO_INCREMENT
, beschreibung VARCHAR(45)
, CONSTRAINT pk PRIMARY KEY (telefontyp_id)
);
CREATE TABLE telefon
(nummer INT
, person_id INT
, telefontyp_id INT
, CONSTRAINT pk PRIMARY KEY (nummer)
, CONSTRAINT telefon_telefontyp FOREIGN KEY (telefontyp_id) REFERENCES telefonTyp (telefontyp_id)
);
CREATE TABLE email
(emailadresse VARCHAR(45)
, person_id INT
, CONSTRAINT pk PRIMARY KEY (emailadresse)
, CONSTRAINT email_person FOREIGN KEY (person_id) REFERENCES person (person_id)
);
CREATE TABLE rolle
(rolle_id INT AUTO_INCREMENT
, bezeichnung VARCHAR(45)
, rabatt BOOLEAN
, CONSTRAINT pk PRIMARY KEY (rolle_id)
);
CREATE TABLE person_rolle
(person_id INT
, rolle_id INT
, CONSTRAINT rolle_person FOREIGN KEY (person_id) REFERENCES person (person_id)
, CONSTRAINT person_rolle FOREIGN KEY (rolle_id) REFERENCES rolle (rolle_id)
);


CREATE TABLE zimmerart
(zimmerart_id INT AUTO_INCREMENT
, typ VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (zimmerart_id)
);
CREATE TABLE zimmer
(zimmer_zimmernr INT
, nichtraucher BOOLEAN
, stockwerk INT
, alpenblick BOOLEAN
, minibar BOOLEAN
, zimmerart_id int
, standort VARCHAR(45)
, zimmergroesseQuadratmeter INT
, CONSTRAINT pkey PRIMARY KEY (zimmer_zimmernr)
, CONSTRAINT zimmer_zimmerart FOREIGN KEY (zimmerart_id) REFERENCES zimmerart (zimmerart_id)
);
CREATE TABLE beschreibung
(beschreibung_id INT AUTO_INCREMENT
, zimmer_zimmernr INT
, CONSTRAINT pkey PRIMARY KEY (beschreibung_id)
, CONSTRAINT beschreibung_zimmer FOREIGN KEY (zimmer_zimmernr) REFERENCES zimmer (zimmer_zimmernr)
);
CREATE TABLE foto
(foto_id INT AUTO_INCREMENT
, name VARCHAR(45)
, url VARCHAR(80)
, beschreibung_id INT
, CONSTRAINT pkey PRIMARY KEY (foto_id)
, CONSTRAINT foto_beschreibung FOREIGN KEY (beschreibung_id) REFERENCES beschreibung (beschreibung_id)
);
CREATE TABLE textbaustein
(textbaustein_id INT AUTO_INCREMENT
, textbaustein VARCHAR(200)
, beschreibung_id INT
, CONSTRAINT pkey PRIMARY KEY (textbaustein_id)
, CONSTRAINT textbaustein_beschreibung FOREIGN KEY (beschreibung_id) REFERENCES beschreibung (beschreibung_id)
);
CREATE TABLE bettart
(bettart_id INT AUTO_INCREMENT
, typ VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (bettart_id)
);
CREATE TABLE zimmer_bettart
(bettart_id INT
, zimmer_zimmernr INT
, CONSTRAINT pkey PRIMARY KEY (bettart_id, zimmer_zimmernr)
);
CREATE TABLE sanitaerobjekt
(sanitaerobjekt_id INT AUTO_INCREMENT
, beschreibung VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (sanitaerobjekt_id)
);
CREATE TABLE sanitaerobjekt_zimmer
(sanitaerobjekt_id INT AUTO_INCREMENT
, zimmer_zimmernr INT
, CONSTRAINT pkey PRIMARY KEY (sanitaerobjekt_id, zimmer_zimmernr)
);

CREATE TABLE kreditkarte
(kreditkartennr INT
, geheimzahl INT
, valid CHAR(5)
, nachname VARCHAR(45)
, vorname VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (kreditkartennr)
);
CREATE TABLE reisegruppe
(reisegruppe_id INT AUTO_INCREMENT
, CONSTRAINT pkey PRIMARY KEY (reisegruppe_id)
);
CREATE TABLE person_reisegruppe
(person_id INT
,reisegruppe_id INT
, CONSTRAINT PKEY PRIMARY KEY (person_id, reisegruppe_id)
, CONSTRAINT person_reisegruppe FOREIGN KEY (person_id) REFERENCES person (person_id)
, CONSTRAINT reisegruppe_person FOREIGN KEY (reisegruppe_id) REFERENCES reisegruppe (reisegruppe_id)
);
CREATE TABLE vermittlungsunternehmenTyp
(vermittlungsunternehmenTyp_id INT AUTO_INCREMENT
, bezeichnung VARCHAR(45)
, CONSTRAINT pkey PRIMARY KEY (vermittlungsunternehmenTyp_id)
);
CREATE TABLE vermittlungsunternehmen
(vermittlungsunternehmen_id INT AUTO_INCREMENT
, bezeichnung VARCHAR(45)
, person_id INT
, vermittlungsunternehmenTyp_id INT
, CONSTRAINT pkey PRIMARY KEY (vermittlungsunternehmen_id)
, CONSTRAINT ansprechsperson FOREIGN KEY (person_id) REFERENCES person (person_id)
, CONSTRAINT vermittlungsunternehmen_vermittlungsunternehmenTyp FOREIGN KEY (vermittlungsunternehmenTyp_id) REFERENCES vermittlungsunternehmenTyp (vermittlungsunternehmenTyp_id)
);
CREATE TABLE person_vermittlungsunternehmen
(vermittlungsunternehmen_id INT
, person_id INT
, CONSTRAINT pkey PRIMARY KEY (vermittlungsunternehmen_id, person_id)
, CONSTRAINT person_vermittlungsunternehmen FOREIGN KEY (person_id) REFERENCES person (person_id)
, CONSTRAINT vermittlungsunternehmen_person FOREIGN KEY (vermittlungsunternehmen_id) REFERENCES vermittlungsunternehmen (vermittlungsunternehmen_id)
);
CREATE TABLE buchung
(buchungsnr INT AUTO_INCREMENT
, buchung_zimmer_zimmernr INT
, buchung_person_id INT
, von DATETIME
, bis DATETIME
, anzahl_personen INT
, bestaetigt BOOLEAN
, buchung_reisegruppe_id INT
, kreditkarte_kreditkartennr INT
, CONSTRAINT pkey PRIMARY KEY (buchungsnr)
, CONSTRAINT buchung_zimmer FOREIGN KEY (buchung_zimmer_zimmernr) REFERENCES zimmer (zimmer_zimmernr)
, CONSTRAINT buchung_person FOREIGN KEY (buchung_person_id) REFERENCES person (person_id)
, CONSTRAINT buchung_reisegruppe FOREIGN KEY (buchung_reisegruppe_id) REFERENCES reisegruppe (reisegruppe_id)
, CONSTRAINT buchung_kreditkarte FOREIGN KEY (kreditkarte_kreditkartennr) REFERENCES kreditkarte (kreditkartennr)
);
CREATE TABLE buchungsstatus (
  buchungsstatus_id INT AUTO_INCREMENT,
  von DATE,
  bis DATE,
  bezeichnung VARCHAR(45),
  buchung_bunchungsnr INT,
  CONSTRAINT pkey PRIMARY KEY (buchungsstatus_id),
  CONSTRAINT buchungsstatus_buchung FOREIGN KEY (buchung_bunchungsnr) REFERENCES buchung (buchungsnr)
  );
